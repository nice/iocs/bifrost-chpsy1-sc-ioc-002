require (rittalpduman2)
# -----------------------------------------------------
#  Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
#epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(chooperpdu_DIR)db/")
epicsEnvSet("IP_ADDRESS","172.30.44.24:502")
epicsEnvSet("PORT", "502")
epicsEnvSet("PREFIX", "P:")
epicsEnvSet("P", "BIFROST-ChpSy1:")
epicsEnvSet("R", "Chop-PDU-001:")
# ------------------------------------------------------------
# Asyn IP Configuration
# ------------------------------------------------------------
#drvAsynIPPortConfigure(portName, hostInfo, priority, noAutoConnect, noProcessEos)
drvAsynIPPortConfigure("Chopper", "$(IP_ADDRESS)", 0,0,1)
# -------------------------------------------------------------
# Modbus configuration
# --------------------------------------------------------------
# modbusInterposeConfig(portName,
#                       linkType,
#                       timeoutMsec,
#                       writeDelayMsec)
modbusInterposeConfig("Chopper",0, 1000,0)
# ---------------------------------------------------------------
# NOTE: We use octal numbers for the start address and length (leading zeros)
# #       to be consistent with the PLC nomenclature.  This is optional, decimal
# #       numbers (no leading zero) or hex numbers can also be used.
#
#drvModbusAsynConfigure(portName, 
#                       tcpPortName,
#                       slaveAddress, 
#                       modbusFunction, 
#                       modbusStartAddress, 
#                       modbusLength,
#                       dataType,
#                       pollMsec, 
#                       plcType);
##
# -----------------------------------------------------------------------------
#  InputRegisterPort
#  InputRegister	0	0	0x0000	11	system identification	RITTAL CMCIII PDU
#  InputRegister	0	11	0x000B	3	mac address	e4:15:f6:e9:f6:5a
#-----------------------------------------------------------------------------
#
 drvModbusAsynConfigure("PDU_Input", "Chopper", 2, 4, 0000, 37, 0, 100,"MODBUS_DATA")
#drvModbusAsynConfigure("PDU_InputM", "Chopper", 0, 4,  0000, 03, 0, 100,"MODBUS_DATA")
#drvModbusAsynConfigure("PDU_IRCur",  "Chopper", 1, 4, 37458, 02, 0, 100,"")
#
drvModbusAsynConfigure("IRReadPort", "Chopper", 2, 4, 37458, 10, 0, 1000,"")
#
drvModbusAsynConfigure("IR_InWord", "Chopper", 2, 4, 37458, 10, 0, 1000,"IRReadPort")

drvModbusAsynConfigure("IRReadL1C", "Chopper", 2, 4, 37458, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL1P", "Chopper", 2, 4, 37703, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL2C", "Chopper", 2, 4, 38535, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL2P", "Chopper", 2, 4, 38780, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL3C", "Chopper", 2, 4, 39612, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadL3P", "Chopper", 2, 4, 39857, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS01C", "Chopper", 2, 4, 40644, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS01P", "Chopper", 2, 4, 40831, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS02C", "Chopper", 2, 4, 41334, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS02P", "Chopper", 2, 4, 41521, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS03C", "Chopper", 2, 4, 42024, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS03P", "Chopper", 2, 4, 42211, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS04C", "Chopper", 2, 4, 42714, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS04P", "Chopper", 2, 4, 42901, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS05C", "Chopper", 2, 4, 43404, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS05P", "Chopper", 2, 4, 43591, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS06C", "Chopper", 2, 4, 44094, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS06P", "Chopper", 2, 4, 44281, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS07C", "Chopper", 2, 4, 44784, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS07P", "Chopper", 2, 4, 44971, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS08C", "Chopper", 2, 4, 45474, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS08P", "Chopper", 2, 4, 45661, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS09C", "Chopper", 2, 4, 46164, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS09P", "Chopper", 2, 4, 46351, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS10C", "Chopper", 2, 4, 46854, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS10P", "Chopper", 2, 4, 47041, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS11C", "Chopper", 2, 4, 47544, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS11P", "Chopper", 2, 4, 47731, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS12C", "Chopper", 2, 4, 48234, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS12P", "Chopper", 2, 4, 48421, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS13C", "Chopper", 2, 4, 48924, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS13P", "Chopper", 2, 4, 49111, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS14C", "Chopper", 2, 4, 49614, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS14P", "Chopper", 2, 4, 49801, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS15C", "Chopper", 2, 4, 50304, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS15P", "Chopper", 2, 4, 50491, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS16C", "Chopper", 2, 4, 50994, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS16P", "Chopper", 2, 4, 51181, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS17C", "Chopper", 2, 4, 51684, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS17P", "Chopper", 2, 4, 51871, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS18C", "Chopper", 2, 4, 52374, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS18P", "Chopper", 2, 4, 52561, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS19C", "Chopper", 2, 4, 53064, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS19P", "Chopper", 2, 4, 53251, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS20C", "Chopper", 2, 4, 53754, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS20P", "Chopper", 2, 4, 53941, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS21C", "Chopper", 2, 4, 54444, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS21P", "Chopper", 2, 4, 54631, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS22C", "Chopper", 2, 4, 55134, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS22P", "Chopper", 2, 4, 55321, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS23C", "Chopper", 2, 4, 55824, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS23P", "Chopper", 2, 4, 56011, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS24C", "Chopper", 2, 4, 56514, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS24P", "Chopper", 2, 4, 56701, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS25C", "Chopper", 2, 4, 57204, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS25P", "Chopper", 2, 4, 57391, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS26C", "Chopper", 2, 4, 57894, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS26P", "Chopper", 2, 4, 58081, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS27C", "Chopper", 2, 4, 58584, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS27P", "Chopper", 2, 4, 58771, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS28C", "Chopper", 2, 4, 59274, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS28P", "Chopper", 2, 4, 59461, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS29C", "Chopper", 2, 4, 59964, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS29P", "Chopper", 2, 4, 60151, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS30C", "Chopper", 2, 4, 60654, 02, 0, 1000,"IRReadPort")
drvModbusAsynConfigure("IRReadS30P", "Chopper", 2, 4, 60841, 02, 0, 1000,"IRReadPort")

# -----------------------------------------------------------------------------
# e3 Common databases, autosave, etc.
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

## Load record instances
#dbLoadTemplate("pdu.substitutions")
dbLoadRecords("aiRead.db","P=$(P), R=$(R)")
dbLoadRecords("calc.db","P=$(P), R=$(R)")
dbLoadRecords("sum.db","P=$(P), R=$(R)")

# -----------------------------------------------------------------------------
# Debug
# -----------------------------------------------------------------------------
#asynSetTraceIOMask("Chopper", 0, 4)
#asynSetTraceIOTruncateSize("Chopper", 0, 512)
#dbLoadTemplate("abb.substitutions")
#iocinit()


